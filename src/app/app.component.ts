import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { logout } from './core/auth/auth.actions';
import { Observable } from 'rxjs';
import { getIsLogged } from './core/auth/auth.selectors';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="home">home</button>
    <button routerLink="users" *ngIf="isLogged$ | async">users</button>
    <button routerLink="pexels-video">pexels-video</button>
    <button (click)="logoutHandler()" *ngIf="isLogged$ | async">logout</button>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  isLogged$: Observable<boolean> = this.store.select(getIsLogged)

  constructor(private store: Store) {
  }

  logoutHandler(): void {
    this.store.dispatch(logout())
  }
}
