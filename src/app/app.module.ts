import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { itemsReducer, ItemsState } from './features/items/store/items.reducer';
import { Item } from './features/items/model/item';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './features/items/store/items.effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { routerReducer, RouterReducerState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/router/router.effects';
import { authReducer, AuthState } from './core/auth/auth.reducer';
import { AuthEffect } from './core/auth/auth.effect';
import { AuthInterceptor } from './core/auth/auth.interceptor';


export interface AppState {
  counter: number;
  router: RouterReducerState;
  authentication: AuthState;
  // items: ItemsState;
}

export const reducers: ActionReducerMap<AppState> = {
  counter: () => 123,
  router: routerReducer,
  authentication: authReducer
  // items: itemsReducer
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([ RouterEffects, AuthEffect ])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, multi: true, useClass: AuthInterceptor}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
