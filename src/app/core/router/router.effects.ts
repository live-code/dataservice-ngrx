import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { back, forward, go } from './router.actions';
import { map, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class RouterEffects {

  go$ = createEffect(() => this.actions$.pipe(
    ofType(go),
    tap(action => {
      this.router.navigateByUrl(action.path);
    })
    // map((action) => this.router.navigateByUrl(action.path))
  ), { dispatch: false });

  back$ = createEffect(() => this.actions$.pipe(
    ofType(back),
    tap(() => this.location.back())
  ), { dispatch: false });

  forward$ = createEffect(() => this.actions$.pipe(
    ofType(forward),
    tap(() => this.location.forward())
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {
  }
}
