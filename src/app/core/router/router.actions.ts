import { createAction, props } from '@ngrx/store';

export const go = createAction(
  '[router] go',
  props<{ path: string}>()
);

export const back = createAction(
  '[router] back',
);

export const forward = createAction(
  '[router] forward',
);
