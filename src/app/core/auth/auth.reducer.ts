import { Auth } from './auth';
import { createReducer, on } from '@ngrx/store';
import { loginFailed, loginSuccess, logout, syncWithStore } from './auth.actions';

export interface AuthState {
  auth: Auth;
  error: boolean;
}

export const initialState = {
  auth: null,
  error: false
}

export const authReducer = createReducer(
  initialState,
  on(syncWithStore, (state, action) => ({...state, auth: action.auth, error: false})),
  on(loginSuccess, (state, action) => ({...state, auth: action.auth, error: false})),
  on(loginFailed, (state, action) => ({...state, error: true})),
  on(logout, (state, action) => ({...state, auth: null})),
);
