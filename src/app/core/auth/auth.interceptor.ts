import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { getIsLogged, getToken } from './auth.selectors';
import { catchError, mergeMap, take, withLatestFrom } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  token: string;
  constructor(private store: Store) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getIsLogged)
      .pipe(
        take(1),
        withLatestFrom(this.store.select(getToken)),
        mergeMap(([isLogged, token]) => {
          const authReq = isLogged ? req.clone({
            setHeaders: { Authorization: 'Bearer ' + token}
          }) : req;
          return next.handle(authReq)
            .pipe(
              catchError(err => {
                if (err instanceof HttpErrorResponse) {
                  switch(err.status) {
                    case 401:
                    case 404:
                    default:
                      this.store.dispatch(go({ path: 'login'}))
                  }

                }
                return throwError(err);
              })
            )
        })
      );
  }
}
