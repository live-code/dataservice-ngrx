import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';

export const getError = (state: AppState) => state.authentication.error;
export const getAuth = (state: AppState) => state.authentication.auth;

export const getIsLogged = createSelector(getAuth, auth => !!auth);
// export const getToken =  (state: AppState) => state.authentication.auth.token;
export const getToken = createSelector(getAuth, auth => auth?.token);
export const getRole = createSelector(getAuth, auth => auth.role);
export const getDisplayName = createSelector(getAuth, auth => auth.displayName);
