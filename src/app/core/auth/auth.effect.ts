import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as AuthActions from './auth.actions';
import { catchError, exhaustMap, filter, map, mapTo, tap } from 'rxjs/operators';
import { Auth } from './auth';
import { of } from 'rxjs';
import { loginFailed } from './auth.actions';
import { go } from '../router/router.actions';
import { Store } from '@ngrx/store';

@Injectable()
export class AuthEffect {

  initEffect$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mapTo(JSON.parse(localStorage.getItem('auth')) as Auth),
    // filter(auth => !!auth),
    map(auth => auth ? AuthActions.syncWithStore({ auth }) : go({ path: 'login'}))
  ))

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.login),
    exhaustMap(action => {
      const params: HttpParams = new HttpParams()
        .set('email', action.email)
        .set('password', action.password);
      return this.http.get<Auth>(`http://localhost:3000/login`, { params})
        .pipe(
          map(auth => AuthActions.loginSuccess({ auth })),
          catchError(() => of(loginFailed()))
        );
    })
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginSuccess),
    tap((action) => localStorage.setItem('auth', JSON.stringify(action.auth))),
    mapTo(go({ path: 'home'}))
  ));

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.logout),
    tap((action) => localStorage.removeItem('auth')),
    mapTo(go({ path: 'login'}))
  ));

  constructor(private actions$: Actions, private http: HttpClient, private store: Store) {
  }
}
