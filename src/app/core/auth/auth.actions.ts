import { createAction, props } from '@ngrx/store';
import { Auth } from './auth';

export const login = createAction(
  '[auth] login',
  props<{ email: string, password: string }>()
);

export const syncWithStore = createAction(
  '[auth] sync localstorage with store',
  props<{ auth: Auth }>()
);

export const loginSuccess = createAction(
  '[auth] login success',
  props<{ auth: Auth }>()
);

export const loginFailed = createAction(
  '[auth] login failed',
);

export const logout = createAction(
  '[auth] logout',
);
