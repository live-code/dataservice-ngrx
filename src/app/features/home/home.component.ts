import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { go } from '../../core/router/router.actions';
import { from, of } from 'rxjs';
import { filter, map, switchMap, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor(private store: Store<AppState>) {

  }

  ngOnInit(): void {
  }

}
