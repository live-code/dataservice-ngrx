import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { login } from '../../core/auth/auth.actions';
import { Observable } from 'rxjs';
import { getError } from '../../core/auth/auth.selectors';

@Component({
  selector: 'app-login',
  template: `
    <div *ngIf="error$ | async">Errore!</div>
    <form #f="ngForm" (submit)="loginHandler(f)">
      <input type="text" name="email" ngModel>
      <input type="password" name="password" ngModel>
      <button type="submit">login</button>
    </form>
  `,
})
export class LoginComponent {
  error$: Observable<boolean> = this.store.select(getError)

  constructor(private store: Store) {
  }

  loginHandler(f: NgForm): void {
    const { email, password } = f.value;
    this.store.dispatch(login({ email, password}));
  }
}
