import { createAction, props } from '@ngrx/store';
import { Item } from '../model/item';

export const loadItems = createAction(
  '[items] load',
);

export const loadItemsSuccess = createAction(
  '[items] load success',
  props<{ items: Item[]}>()
);

export const loadItemsFailed = createAction(
  '[items] load failed',
);

export const saveItem = createAction(
  '[item] save',
  props<{ item: Item }>()
);

export const addItem = createAction(
  '[item] add',
  props<{ item: Item }>()
);

export const addItemSuccess = createAction(
  '[item] add success',
  props<{ item: Item }>()
);

export const addItemFailed = createAction(
  '[items] add failed',
);



export const editItem = createAction(
  '[item] edit',
  props<{ item: Item }>()
);

export const editItemSuccess = createAction(
  '[item] edit success',
  props<{ item: Item }>()
);

export const editItemFailed = createAction(
  '[items] edit failed',
);


export const deleteItem = createAction(
  '[item] delete',
  props<{ id: number }>()
);

export const deleteItemSuccess = createAction(
  '[item] delete success',
  props<{ id: number }>()
);

export const deleteItemFailed = createAction(
  '[item] delete failed',
);

export const setActive = createAction(
  '[item] set active',
  props<{ item: Item }>()
);

export const cleanActive = createAction(
  '[item] set active clean',
);
