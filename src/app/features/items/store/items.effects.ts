import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ItemsActions from './items.actions';
import { catchError, concatMap, delay, exhaustMap, filter, map, mapTo, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Item } from '../model/item';
import { of } from 'rxjs';
import { go } from '../../../core/router/router.actions';
import { addItem, editItem, saveItem } from './items.actions';
import { Store } from '@ngrx/store';
import { getActiveItem } from './items.selectors';

@Injectable()
export class ItemsEffects {
  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/users')
        .pipe(
          switchMap(items => [
            ItemsActions.loadItemsSuccess({ items }),
            // go({ path: 'home'})
          ]),
          // map(items => go({ path: 'home'})),
          catchError(() => of(ItemsActions.loadItemsFailed()))
        )
    )
  ));
/*
 loadItemsSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.loadItemsSuccess),
    // map((action) => go({ path: 'home'}))
    mapTo(go({ path: 'home'}))
  ));*/

  saveItem$ = createEffect(() => this.actions$.pipe(
    ofType(saveItem),
    withLatestFrom(this.store.select(getActiveItem)),
    map(([action, active])  => active ?
          editItem({ item: { ...action.item, id: active.id }}) :
          addItem({ item: action.item}))
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    // filter(action => action.type === addItem.type)
    ofType(ItemsActions.addItem),
    concatMap(
      action => this.http.post<Item>('http://localhost:3000/users', action.item)
        .pipe(
          delay(1000),
          map(item => ItemsActions.addItemSuccess({ item })),
          catchError(() => of(ItemsActions.addItemFailed()))
        )
    )
  ));

  editItem$ = createEffect(() => this.actions$.pipe(
    // filter(action => action.type === addItem.type)
    ofType(ItemsActions.editItem),
    concatMap(
      action => this.http.patch<Item>('http://localhost:3000/users/' + action.item.id, action.item)
        .pipe(
          delay(1000),
          map(item => ItemsActions.editItemSuccess({ item })),
          catchError(() => of(ItemsActions.editItemFailed()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.deleteItem),
    mergeMap(
      ({ id }) => this.http.delete(`http://localhost:3000/users/${id}`)
        .pipe(
          delay(1000),
          map(() => ItemsActions.deleteItemSuccess({ id })),
          catchError(() => of(ItemsActions.deleteItemFailed()))
        )
    )
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store
    ) {
  }
}
