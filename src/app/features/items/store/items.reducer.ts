import { createReducer, on } from '@ngrx/store';
import { Item } from '../model/item';
import * as ItemsActions from './items.actions';

export interface ItemsState {
  active: Item;
  list: Item[];
  hasError: boolean;
  pending: boolean;
}
const initialState: ItemsState = {
  active: null,
  list: [],
  hasError: false,
  pending: false
};

export const itemsReducer = createReducer(
  initialState,
  on(ItemsActions.setActive, (state, action) => ({...state, active: action.item})),
  on(ItemsActions.cleanActive, (state, action) => ({...state, active: null})),
  on(ItemsActions.loadItems, (state, action) => {
    return {...state, hasError: false, pending: true};
  }),
  on(ItemsActions.loadItemsSuccess, (state, action) => {
    return { ...state, list: [...action.items], pending: false };
  }),
  on(ItemsActions.loadItemsFailed, (state, action) => {
    return { ...state, hasError: true, pending: false };
  }),
  on(ItemsActions.addItem, (state, action) => {
    return {...state, hasError: false, pending: true};
  }),
  on(ItemsActions.addItemSuccess, (state, action) => {
    return { ...state, list: [...state.list, action.item], pending: false };
  }),

  on(ItemsActions.editItemSuccess, (state, action) => {
    return {
      ...state,
      list: state.list.map(item => item.id === action.item.id ? action.item : item ),
      pending: false };
  }),

  on(ItemsActions.addItemFailed, (state, action) => {
    return { ...state, hasError: true, pending: false };
  }),
  on(ItemsActions.deleteItem, (state, action) => {
    return {...state, hasError: false, pending: true};
  }),
  on(ItemsActions.deleteItemSuccess, (state, action) => {
    return {
      ...state,
      list: state.list.filter(item => item.id !== action.id),
      pending: false,
      active: state.active?.id === action.id ? null : state.active
    }
  }),
  on(ItemsActions.deleteItemFailed, (state, action) => {
    return { ...state, hasError: true, pending: false };
  }),
);

