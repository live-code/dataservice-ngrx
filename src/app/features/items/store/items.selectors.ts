import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersState } from '../items.module';

export const getUsersFeature = createFeatureSelector('users');

export const getActiveItem = createSelector(
  getUsersFeature,
  (state: UsersState) => state.items.active
);

export const getItems = createSelector(
  getUsersFeature,
  (state: UsersState) => state.items.list
);

export const getItemsError = createSelector(
  getUsersFeature,
  (state: UsersState) => state.items.hasError
);

export const getItemsPending =  createSelector(
  getUsersFeature,
  (state: UsersState) => state.items.pending
);
