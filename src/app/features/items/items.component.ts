import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Item } from './model/item';
import { select, Store } from '@ngrx/store';
import { getActiveItem, getItems, getItemsError, getItemsPending } from './store/items.selectors';
import { Actions, ofType } from '@ngrx/effects';
import { addItem, addItemSuccess, cleanActive, deleteItem, loadItems, saveItem, setActive } from './store/items.actions';

@Component({
  selector: 'app-items',
  template: `
    <div class="alert alert-danger" *ngIf="error$ | async">errore!!</div>
    <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"
       *ngIf="pending$ | async"></i>

    <form #f="ngForm" (submit)="saveItemHandler(f)">
      <input type="text" [ngModel]="active?.name" name="name">
      <button type="submit">
        {{active ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="resetHandler(f)">CLEAN</button>
    </form>

    <hr>
    
    <li
      class="list-group-item"
      *ngFor="let item of (items$ | async)"
      (click)="setActiveHandler(item)"
      [ngClass]="{'active': item.id === active?.id}"
    >
      {{item.name}}
      <button (click)="deleteItemHandler(item, $event)">delete</button>
    </li>
  `,
})
export class ItemsComponent implements OnDestroy {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  active$: Observable<Item> = this.store.pipe(select(getActiveItem));
  error$: Observable<boolean> = this.store.select(getItemsError)
  pending$: Observable<boolean> = this.store.select(getItemsPending)
  active: Item;
  activeSub: Subscription;

  constructor(private store: Store, private actions$: Actions) {
    this.loadItemsHandler();

    this.activeSub = this.active$.subscribe(val => this.active = val)
    actions$
      .pipe(ofType(addItemSuccess))
      .subscribe(() => {
        this.form.reset();
      });
  }

  saveItemHandler(form: NgForm): void {
    /*if(this.active){
      console.log('edit')
    } else {
      this.store.dispatch(addItem({ item: form.value }));
    }*/
    this.store.dispatch(saveItem({ item: form.value}))
  }

  deleteItemHandler(item: Item, e: MouseEvent): void {
    e.stopPropagation();
    this.store.dispatch(deleteItem({ id: item.id }));
  }

  loadItemsHandler(): void  {
    this.store.dispatch(loadItems())
  }

  setActiveHandler(item: Item): void {
    this.store.dispatch(setActive({ item }))
  }

  ngOnDestroy(): void {
    this.activeSub.unsubscribe();
  }

  resetHandler(f: NgForm): void {
    this.store.dispatch(cleanActive())
    f.reset();
  }
}
