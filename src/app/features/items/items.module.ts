import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { FormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { itemsReducer, ItemsState } from './store/items.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';

export interface UsersState {
  items: ItemsState;
}

export const reducers: ActionReducerMap<UsersState> = {
  items: itemsReducer
};

@NgModule({
  declarations: [ItemsComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    FormsModule,
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature([ItemsEffects])
  ]
})
export class ItemsModule { }
