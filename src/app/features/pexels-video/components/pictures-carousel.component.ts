import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pictures-carousel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    {{currentImage}}
    <br>
    <img [src]="currentImage" (load)="completeLoading()">
  `,
})
export class PicturesCarouselComponent implements OnChanges{
  @Input() images: string[];
  currentImage: string;
  index = 0;
  timerId: any;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnChanges(): void {
    if (this.images) {
     this.index = 0;
     this.currentImage = this.images[this.index];
    }
  }

  completeLoading(): void {
    this.index = this.index < this.images.length - 1 ?
      ++this.index :
      0;

    clearTimeout(this.timerId);
    this.timerId = setTimeout(() => {
      this.currentImage = this.images[this.index];
      this.cd.detectChanges();
    }, 5000);
  }
}
