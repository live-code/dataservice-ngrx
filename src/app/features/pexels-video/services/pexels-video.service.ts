import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PexelsVideoResponse } from '../model/pexels-video-response';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PexelsVideoService {
  searchVideo(text: string): Observable<PexelsVideoResponse> {
    return this.http.get<PexelsVideoResponse>(
      `https://api.pexels.com/videos/search?query=${text}&per_page=15&page=1`,
      { headers: { Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'}}
    );
  }

  constructor(private http: HttpClient) {
  }
}
