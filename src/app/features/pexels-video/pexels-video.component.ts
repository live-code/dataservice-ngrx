import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { searchVideo } from './store/search/search.actions';
import { Observable } from 'rxjs';
import { Video, VideoPicture } from './model/pexels-video-response';
import {
  getPexelsVideos,
  getPexelsVideosByRes,
  getPexelsVideosError,
  getPexelsVideosPending,
  getPexelsVideosPictures
} from './store/search/search.selectors';
import { showVideo } from './store/player/player.action';
import { getCurrentVideoURL } from './store/player/player.selectors';
import { filterByRes } from './store/filter/filter.actions';
import { getPexelsVideoFiltersResolution } from './store/filter/filter.selectors';

@Component({
  selector: 'app-pexels-video',
  template: `
    
    <hr>
    <div class="alert alert-danger" *ngIf="error$ | async">Errore!</div>
    <div class="alert alert-info" *ngIf="pending$ | async">Loading...</div>
    
    <video *ngIf="videoUrl$ | async as video" [src]="video" width="100" controls>
      No support for videos
    </video>
    <input 
      type="text" class="form-control" placeholder="search videos..."
      (keydown.enter)="searchVideoHandler($event)"
    >
    
    <div *ngIf="!(videos$ | async).length">no videos</div>
    
    <select [ngModel]="minResolution$ | async" (change)="filterByResHandler($event)">
      <option>0</option>
      <option>2000</option>
      <option>4000</option>    
    </select>
    
    <div class="d-flex flex-wrap">
      <div *ngFor="let video of videos$ | async">
        <img 
          [src]="video.image" width="100" [alt]="'by ' + video.user?.name"
          (click)="showPreview(video)"
        >
      </div>
    </div>
    <hr>

    <app-pictures-carousel
      [images]="videoPictures$ | async"
    ></app-pictures-carousel>
  
  `,
})
export class PexelsVideoComponent  {
  videos$: Observable<Video[]> = this.store.select(getPexelsVideosByRes);
  error$: Observable<boolean> = this.store.select(getPexelsVideosError);
  pending$: Observable<boolean> = this.store.select(getPexelsVideosPending);
  videoPictures$: Observable<string[]> = this.store.select(getPexelsVideosPictures);
  videoUrl$: Observable<string> = this.store.select(getCurrentVideoURL);
  minResolution$: Observable<number> = this.store.select(getPexelsVideoFiltersResolution);

  constructor(private store: Store) {
    this.store.dispatch(searchVideo({ text: 'girls' }));
  }

  searchVideoHandler(e: KeyboardEvent): void {
    const text = (e.target as HTMLInputElement).value;
    this.store.dispatch(searchVideo({ text }));
  }

  showPreview(video: Video): void {
    this.store.dispatch(showVideo({ video }));
  }

  filterByResHandler(event: Event): void {
    const minRes = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByRes({ minRes }))

  }
}
