import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PexelsVideoComponent } from './pexels-video.component';

const routes: Routes = [{ path: '', component: PexelsVideoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PexelsVideoRoutingModule { }
