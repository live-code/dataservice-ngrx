import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { PexelsVideoSearchState, searchReducer } from './store/search/search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './store/search/search.effects';
import { PexelsPlayerState, playerReducer } from './store/player/player.reducer';
import { PicturesCarouselComponent } from './components/pictures-carousel.component';
import { FormsModule } from '@angular/forms';
import { filterReducer, PexelsVideoFilterState } from './store/filter/filter.reducer';

export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  filter: PexelsVideoFilterState;
}

export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchReducer,
  player: playerReducer,
  filter: filterReducer
}

@NgModule({
  declarations: [PexelsVideoComponent, PicturesCarouselComponent],
  imports: [
    CommonModule,
    FormsModule,
    PexelsVideoRoutingModule,
    StoreModule.forFeature('pexels-video', reducers),
    EffectsModule.forFeature([SearchEffects])
  ]
})
export class PexelsVideoModule { }
