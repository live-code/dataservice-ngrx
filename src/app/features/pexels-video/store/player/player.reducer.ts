import { createReducer, on } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import { showVideo } from './player.action';
import { searchVideo } from '../search/search.actions';

export interface PexelsPlayerState {
  currentVideo: Video;
}
const initialState = {
  currentVideo: null
};

export const playerReducer = createReducer(
  initialState,
  on(showVideo, (state, action) => ({ currentVideo: action.video})),
  on(searchVideo, () => ({ currentVideo: null}))
)
