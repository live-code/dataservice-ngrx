import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';
import { getPexelsVideoFeature } from '../index';

export const getCurrentVideo = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo
);

export const getCurrentVideoID = createSelector(
  getCurrentVideo,
  state => state?.id
);

export const getCurrentVideoURL = createSelector(
  getCurrentVideo,
  state => state?.video_files[0].link
);
