import { createAction, props } from '@ngrx/store';

export const filterByRes = createAction(
  '[pexels-video] filter',
  props<{ minRes: number}>()
);
