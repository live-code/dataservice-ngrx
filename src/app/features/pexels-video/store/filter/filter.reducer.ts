import { createReducer, on } from '@ngrx/store';
import { filterByRes } from './filter.actions';
import { searchVideo } from '../search/search.actions';

export interface PexelsVideoFilterState {
  minResolution: number;
}
const initialState = {
  minResolution: 0
}

export const filterReducer = createReducer(
  initialState,
  on(filterByRes, (state, action) => ({ ...state, minResolution: action.minRes})),
  on(searchVideo, (state) => ({ ...state, minResolution: 0}))
);

