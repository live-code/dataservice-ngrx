import { createSelector } from '@ngrx/store';
import { getPexelsVideoFeature } from '../index';


export const getPexelsVideoFilters = createSelector(
  getPexelsVideoFeature,
  state => state.filter
);

export const getPexelsVideoFiltersResolution = createSelector(
  getPexelsVideoFilters,
  state => state.minResolution
);

