import { createFeatureSelector } from '@ngrx/store';
import { PexelsVideoState } from '../pexels-video.module';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video');
