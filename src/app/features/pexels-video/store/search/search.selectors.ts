import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getCurrentVideoID } from '../player/player.selectors';
import { getPexelsVideoFiltersResolution } from '../filter/filter.selectors';
import { getPexelsVideoFeature } from '../index';


export const getPexelsVideos = createSelector(
  getPexelsVideoFeature,
  state => state.search.list
);

export const getPexelsVideosByRes = createSelector(
  getPexelsVideos,
  getPexelsVideoFiltersResolution,
  (videos, minRes) => videos.filter(v => v.width > minRes)
);

export const getPexelsVideosError = createSelector(
  getPexelsVideoFeature,
  state => state.search.hasError
);
export const getPexelsVideosPending = createSelector(
  getPexelsVideoFeature,
  state => state.search.pending
);
export const getPexelsVideosPictures = createSelector(
  getPexelsVideos,
  getCurrentVideoID,
  (videoList, currentVideoID) => videoList.find(v => v.id === currentVideoID)?.video_pictures
    .map(videoPicture => videoPicture.picture)
);
/*
export const getPexelsVideosPictures = createSelector(
  getPexelsVideoFeature,
  state => state.search.list.find(v => v.id === state.player.currentVideo?.id)?.video_pictures
    .map(videoPicture => videoPicture.picture)
);
*/
