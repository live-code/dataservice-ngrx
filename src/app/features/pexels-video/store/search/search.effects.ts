import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { searchVideo, searchVideoFailed, searchVideoSuccess } from './search.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { PexelsVideoService } from '../../services/pexels-video.service';
import { of } from 'rxjs';

@Injectable()
export class SearchEffects {
  searchVideo$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideo),
    switchMap(
      action => this.pexelService.searchVideo(action.text)
        .pipe(
          map(response => searchVideoSuccess({ items: response.videos })),
          catchError(() => of(searchVideoFailed()))
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private pexelService: PexelsVideoService
  ) {}
}
