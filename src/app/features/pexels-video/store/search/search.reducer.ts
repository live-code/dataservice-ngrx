import { createReducer, on } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import { searchVideo, searchVideoFailed, searchVideoSuccess } from './search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
}

const initialState: PexelsVideoSearchState = {
  list: [],
  hasError: false,
  pending: false,
};

export const searchReducer = createReducer(
  initialState,
  on(searchVideo, state => ({...state, pending: true, hasError: false})),
  on(searchVideoSuccess, (state, action) => ({...state, pending: false, list: action.items })),
  on(searchVideoFailed, (state) => ({...state, pending: false, hasError: true })),
);
