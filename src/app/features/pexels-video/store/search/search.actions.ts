import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const searchVideo = createAction(
  '[pexels-video]: search',
  props<{ text: string }>()
);

export const searchVideoSuccess = createAction(
  '[pexels-video]: search success',
  props<{ items: Video[] }>()
);

export const searchVideoFailed = createAction(
  '[pexels-video]: search failed',
);
